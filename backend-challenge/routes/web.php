<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
//unauthenticated routes...
$router->post('/login', ['as' => 'login', 'uses' => 'LoginController@login']);
$router->get('/logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);

//panel - authenticated routes
$router->group(['middleware' => 'auth:api'], function () use ($router) {
    //login jwt refresh
    Route::put('/refresh', 'LoginController@refresh');
    //users routes
    $router->group(['prefix' => 'users'], function () use ($router) {
        $router->get('/', ['as' => 'users', 'uses' => 'UsersController@index']);
        $router->post('/add', ['as' => 'add_user', 'uses' => 'UsersController@add']);
        $router->post('/update/{id}', ['as' => 'update_user', 'uses' => 'UsersController@update']);
        $router->get('/delete/{id}', ['as' => 'delete_user', 'uses' => 'UsersController@delete']);
    });
    //products routes
    $router->group(['prefix' => 'products'], function () use ($router) {
        $router->get('/', ['as' => 'products', 'uses' => 'ProductsController@index']);
        $router->post('/add', ['as' => 'add_product', 'uses' => 'ProductsController@add']);
        $router->post('/update/{id}', ['as' => 'update_product', 'uses' => 'ProductsController@update']);
        $router->get('/delete/{id}', ['as' => 'delete_product', 'uses' => 'ProductsController@delete']);
    });
});
