<link rel="stylesheet" type="text/css" href="{{url('public/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('public/css/bootstrap-grid.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('public/css/bootstrap-reboot.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('public/css/jquery-ui.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('public/css/style.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('public/css/fa-brands.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('public/css/fa-regular.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('public/css/fa-solid.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('public/css/fontawesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('public//css/fontawesome-all.min.css')}}">
<script src="{{url('public//js/jquery.min.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{url('public//js/jquery.mask.min.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{url('public/js/jquery-ui.min.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{url('public/js/bootstrap.min.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{url('public/js/js.cookie.min.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{url('public/js/app.js')}}" type="text/javascript" charset="utf-8"></script>
@yield('load_assets')
