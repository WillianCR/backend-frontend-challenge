<div class="row">
    <div class="container">
        <div class="row">
            @if(session()->has('access_token'))
            <div class="col-md-12" style="display: contents">
                <div class="sidebar col-md-3">
                    <a class="btn @if(request()->path() == 'users') btn-primary @else btn-outline-primary @endif  btn-block link" href="{{route('users_list')}}">Users</a>
                    <a class="btn @if(request()->path() == 'products') btn-primary @else btn-outline-primary @endif btn-block link" href="{{route('products_list')}}">Products</a>
                </div>
                <div class="content-data col-md-9">
                    @yield('content')
                </div>
            </div>
            @else
                @yield('content')
            @endif
        </div>
    </div>
</div>
