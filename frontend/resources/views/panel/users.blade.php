@extends('template.template')
@section('load_assets')
    <script src="{{url('public/js/actions.js')}}" type="text/javascript" charset="utf-8"></script>
@endsection
@section('content')
    <div class="" style="display: inline-block; margin-top: 10px;">
        <div class="col-md-12" >
            <div class="portlet-body form">
                                <form method="get" action="{{url()->current()}}">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="title">Name</label>
                                                    <input type="text" name="name" id="name" value="{{request('name')}}" class="form-control name" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="description">Email</label>
                                                    <input type="text" name="email" id="email" value="{{request('email')}}" class="form-control email" />
                                                </div>
                                            </div>
                                            <div class="col-md-1 btn-same-line">
                                                <button type="submit" class="btn btn-success" title="Search">
                                                    <i class="fa fa fa-search" style="padding-right: 5px;"></i>Search
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="event-list">
                        <div style="margin-bottom: 10px; display: flow-root">
                            {{csrf_field()}}
                            <span class="event-list-span"><i class="fa fa-calendar"></i> Event list</span>
                            <a style="float: right;margin-right:5px;" class="btn btn-dark new-event" href="{{route('users_add')}}"><i class="fa fa-plus"></i> Add User</a>
                        </div>
                        <div class="portlet-body">
                            <div class="table-div">
                                <table class="table table-striped table-bordered table-advance table-hover">
                                    <caption>
                                        <span>Total: {{count($users->data)}}</span>
                                    </caption>
                                    <thead>
                                    <tr>
                                        <th class="text-center" width="">Name</th>
                                        <th class="text-center" width="">Email</th>
                                        <th class="text-center" width="">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users->data as $user)
                                        <tr>
                                            <td class="text-center">{{$user->name}}</td>
                                            <td class="text-center">{{$user->email}}</td>
                                            <td class="text-center">
                                                <button data-href="{{route('users_edit', ['id_user' => $user->id])}}" class="bt_edit btn-primary btn-sm" title="Edit"><i class="fa fa-pencil-alt"></i></button>
                                                <button data-href="{{route('users_delete', ['id_user' => $user->id])}}" data-id-event="{{$user->id}}" class="bt_delete btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="text-center">

                                </div>
                            </div>
                        </div>

@endsection
