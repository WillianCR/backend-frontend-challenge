@extends('template.template')
@section('load_assets')
    <script src="{{url('public/js/actions.js')}}" type="text/javascript" charset="utf-8"></script>
@endsection

@section('content')
                    <div class="col-md-12 event-list">
                        <div class="">
                            <span class="event-list-span"><i class="fa fa-calendar"></i> User Form</span>
                        </div>
                        <form method="post" @if(isset($user)) action="{{route('users_edit_submit', ['id_user' => $user->data[0]->id])}}" @else action="{{route('users_add_submit')}}" @endif >
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <button class="close" data-close="alert"></button>
                                    @foreach ($errors->all() as $error)
                                        <span>{{$error}}</span><br>
                                    @endforeach
                                </div>
                            @endif
                            {{csrf_field()}}
                            @if(isset($user))
                                <input type="hidden" name="id_event" value="{{$user->data[0]->id}}"/>
                            @endif
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label class="control-label" for="name">Name</label>
                                        <input required @if(isset($user)) value="{{$user->data[0]->name}}" @endif id="name" name="name" maxlength="50" type="text" class="form-control">
                                    </div>
                                    <div class="col-md-12 form-group">
                                        <label class="control-label" for="email">Email</label>
                                        <input required @if(isset($user)) value="{{$user->data[0]->email}}" @endif id="email" maxlength="50" name="email" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group">
                                        <label class="control-label" for="password">Password</label>
                                        <input @if(!isset($user)) required @endif id="password" minlength="6" maxlength="30" name="password" type="password" class="form-control password">
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <label class="control-label" for="password_confirmation">Confirm</label>
                                        <input @if(!isset($user)) required @endif id="password_confirmation" minlength="6" maxlength="30" name="password_confirmation" type="password" class="form-control password_confirmation">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <button style="float: right" class="btn btn-primary" type="submit"><i class="fa fa-plus"></i> @if(isset($user)) Edit @else Add @endif User</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
@endsection
