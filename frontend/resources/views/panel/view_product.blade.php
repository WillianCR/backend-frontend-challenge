@extends('template.template')
@section('load_assets')
    <script src="{{url('public/js/actions.js')}}" type="text/javascript" charset="utf-8"></script>
@endsection

@section('content')
    <div class="col-md-12 event-list">
        <div class="">
            <span class="event-list-span"><i class="fa fa-calendar"></i> Product Form</span>
        </div>
        <form method="post" @if(isset($product)) action="{{route('products_edit_submit', ['id_product' => $product->data[0]->id])}}" @else action="{{route('products_add_submit')}}" @endif >
            @if ($errors->any())
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    @foreach ($errors->all() as $error)
                        <span>{{$error}}</span><br>
                    @endforeach
                </div>
            @endif
            {{csrf_field()}}
            @if(isset($product))
                <input type="hidden" name="id_event" value="{{$product->data[0]->id}}"/>
            @endif
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12 form-group">
                        <label class="control-label" for="name">Name</label>
                        <input required @if(isset($product)) value="{{$product->data[0]->name}}" @endif id="name" name="name" maxlength="50" type="text" class="form-control">
                    </div>
                    <div class="col-md-12 form-group">
                        <label class="control-label" for="description">Description</label>
                        <input required @if(isset($product)) value="{{$product->data[0]->description}}" @endif id="description" maxlength="50" name="description" type="text" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 form-group">
                        <label class="control-label" for="quantity">Quantity</label>
                        <input required @if(isset($product)) value="{{$product->data[0]->quantity}}" @endif id="quantity" min="0" maxlength="100000" name="quantity" type="number" class="form-control quantity">
                    </div>
                    <div class="col-md-4 form-group">
                        <label class="control-label" for="price">Price U$</label>
                        <input required @if(isset($product)) value="{{$product->data[0]->price}}" @endif name="price" id="price" type="number" step="0.01" min="0.01" max="999999.99" class="form-control price">
                    </div>
                    <div class="col-md-4 form-group">
                        <label class="control-label" for="color">Color</label>
                        <select id="color" name="color" class="form-control">
                            <option value="">Select optional</option>
                            <option @if(isset($product) && $product->data[0]->color == 'blue') selected @endif style="color: white; background-color: blue" value="blue">blue</option>
                            <option @if(isset($product) && $product->data[0]->color == 'red') selected @endif style="color: white; background: red" value="red">red</option>
                            <option @if(isset($product) && $product->data[0]->color == 'yellow') selected @endif style="color: black; background: yellow" value="yellow">yellow</option>
                            <option @if(isset($product) && $product->data[0]->color == 'green') selected @endif style="color: white; background: green" value="green">green</option>
                            <option @if(isset($product) && $product->data[0]->color == 'purple') selected @endif style="color: white; background: purple" value="purple">purple</option>
                            <option @if(isset($product) && $product->data[0]->color == 'brown') selected @endif style="color: white; background: brown" value="brown">brown</option>
                            <option @if(isset($product) && $product->data[0]->color == 'white') selected @endif style="color: black; background: white" value="white">white</option>
                            <option @if(isset($product) && $product->data[0]->color == 'black') selected @endif style="color: white; background: black" value="black">black</option>
                            <option @if(isset($product) && $product->data[0]->color == 'magenta') selected @endif style="color: white; background: magenta" value="magenta">magenta</option>
                            <option @if(isset($product) && $product->data[0]->color == 'grey') selected @endif style="color: white; background: grey" value="grey">grey</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button style="float: right" class="btn btn-primary" type="submit"><i class="fa fa-plus"></i> @if(isset($product)) Edit @else Add @endif Product</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
