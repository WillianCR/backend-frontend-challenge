<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use http\Client\Response;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    /**
     ** Login form
     * @return view
     **/
    public function index()
    {
        $title = 'Dashboard';
        return view('panel.dashboard', compact('title'));
    }
}
