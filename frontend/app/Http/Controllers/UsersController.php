<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function index(Request $request){
        $title = 'Users List';

        $users = $this->getApi('/users');
        return view('panel.users', compact('title', 'users'));
    }

    public function add(){
        $title = 'Add User';
        return view('panel.view_user', compact('title'));
    }

    public function edit($id_user){
        $title = 'Edit User';

        $user = $this->getApi('/users?id='.$id_user);
        return view('panel.view_user', compact('title', 'user'));
    }

    public function delete($id_user){
        $user_deletion = $this->getApi('/users/delete/'.$id_user);
    }

    public function submit(Request $request, $id_user = null){

        $url = '/users/add';
        $password_rule = 'required|string|min:6|confirmed';
        if($id_user){
            $password_rule = 'confirmed';
            $url = '/users/update/'.$id_user;
        }


        $this->validate($request, [
            'name' => 'required|string|max:50',
            'email' => 'required|email|max:50',
            'password' => $password_rule,
        ]);

        $response = $this->postApi($url, $request->only('name', 'email', 'password', 'password_confirmation'));

        if(isset($response->errors))
            return redirect()->back()->withErrors($response->errors);
        return redirect()->route('users_edit', ['id_user' => $response->user->id]);

    }

}
