<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use GuzzleHttp\Client;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Get Api
     * @return json
     * @param $url url
     **/
    protected function getApi($url)
    {
        $client = new Client(['http_errors' => false]);
        if(request()->query)
            $url .= '?'.request()->getQueryString();
        $response = $client->request('GET', env('API_URL').$url, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.session('access_token')
            ]
        ]);
        return json_decode($response->getBody());
    }
    /**
     * Post Api
     * @return json
     * @param $url url
     * @param $data data
     **/
    protected function postApi($url, $data)
    {
        $client = new Client(['http_errors' => false]);
        $response = $client->request('POST', env('API_URL').$url, [
            'body' => json_encode($data),
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer '.session('access_token')
            ]
        ]);

        if($response->getStatusCode() == 422)
            return json_decode(json_encode(['errors' => json_decode($response->getBody())]));
        return json_decode($response->getBody());
    }
}
