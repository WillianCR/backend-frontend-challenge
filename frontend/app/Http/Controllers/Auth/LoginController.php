<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use http\Client\Response;
use Illuminate\Http\Request;
use GuzzleHttp\Client;

class LoginController extends Controller
{
    /**
     ** Login form
     * @return view
     **/
    public function index()
    {
        $title = 'Login';

        if(session('access_token'))
            session()->forget('access_token');

        return view('login', compact('title'));
    }

    /**
     ** Login submit
     **
     * @param Request $request
     * @return response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {   
        if(session('access_token'))
            session()->forget('access_token');
        
        $this->validate($request, [
            'email' => 'required|email|max:50',
            'password' => 'required|string|min:6',
        ]);

        $client = new Client();
        $response = $client->request('POST', env('API_URL').'/login', [
            'body' => json_encode($request->only('email', 'password')),
            'headers' => [
                'Content-Type' => 'application/json'
            ]
        ]);
        $body = json_decode($response->getBody());
        if(isset($body->access_token)){
            session()->put('access_token', $body->access_token);
            session()->put('access_token_expires', $body->expires_in);
            return redirect('/');
        }

        return redirect('login')->withErrors($body);
    }

    /**
     ** Login form
     * @return view
     **/
    public function refresh()
    {
        if(session('jwt_token'))
            session()->forget('jwt_token');

        return view('login');
    }
}
