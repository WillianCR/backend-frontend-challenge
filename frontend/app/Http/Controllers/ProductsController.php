<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(Request $request){
        $title = 'Products List';

        $products = $this->getApi('/products');
        return view('panel.products', compact('title', 'products'));
    }

    public function add(){
        $title = 'Add Product';
        return view('panel.view_product', compact('title'));
    }

    public function edit($id_product){
        $title = 'Edit Product';

        $product = $this->getApi('/products?id='.$id_product);
        return view('panel.view_product', compact('title', 'product'));
    }

    public function delete($id_product){
        $product_deletion = $this->getApi('/products/delete/'.$id_product);
    }

    public function submit(Request $request, $id_product = null){

        $url = '/products/add';
        if($id_product)
            $url = '/products/update/'.$id_product;

        $this->validate($request, [
            'name' => 'required|string|max:30',
            'description' => 'required|max:50|string',
            'quantity' => 'required|numeric|min:0',
            'price' => 'required|numeric',
            'color' => 'nullable|in:blue,red,green,purple,yellow,brown,black,white,magenta,grey'
        ]);

        $response = $this->postApi($url, $request->only('name', 'description','quantity', 'price', 'color'));

        if(isset($response->errors))
            return redirect()->back()->withErrors($response->errors);
        return redirect()->route('products_edit', ['id_product' => $response->product->id]);

    }
}
