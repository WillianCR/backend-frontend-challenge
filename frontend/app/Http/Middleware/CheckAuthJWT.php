<?php

namespace App\Http\Middleware;

use Closure;

class CheckAuthJWT
{
    /**
     * Handle an incoming request.
     * If having JWT cookie continue request, but if d'ont redirect to login
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has('access_token'))
            return redirect('/login');
        return $next($request);
    }
}
