<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//unauthenticated routes...
Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@index']);
Route::post('/login', ['as' => 'send_login', 'uses' => 'Auth\LoginController@login']);

//panel - authenticated routes
Route::group(['middleware' => 'have_cookie_auth'], function () {
    //home
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    //login jwt refresh
    Route::get('/jwt/refresh', 'LoginController@refresh');
    //products
    Route::get('/products', ['as' => 'products_list', 'uses' => 'ProductsController@index']);
    Route::get('/product/add', ['as' => 'products_add', 'uses' => 'ProductsController@add']);
    Route::post('/product/add', ['as' => 'products_add_submit', 'uses' => 'ProductsController@submit']);
    Route::get('/product/edit/{id_product}', ['as' => 'products_edit', 'uses' => 'ProductsController@edit']);
    Route::post('/product/edit/{id_product}', ['as' => 'products_edit_submit', 'uses' => 'ProductsController@submit']);
    Route::get('/product/delete/{id_product}', ['as' => 'products_delete', 'uses' => 'ProductsController@delete']);

    //users
    Route::get('/users', ['as' => 'users_list', 'uses' => 'UsersController@index']);
    Route::get('/user/add', ['as' => 'users_add', 'uses' => 'UsersController@add']);
    Route::post('/user/add', ['as' => 'users_add_submit', 'uses' => 'UsersController@submit']);
    Route::get('/user/edit/{id_user}', ['as' => 'users_edit', 'uses' => 'UsersController@edit']);
    Route::post('/user/edit/{id_user}', ['as' => 'users_edit_submit', 'uses' => 'UsersController@submit']);
    Route::get('/user/delete/{id_user}', ['as' => 'users_delete', 'uses' => 'UsersController@delete']);

});
