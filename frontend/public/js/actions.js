$(function(){
  //edit event
  $('.bt_edit').click(function(e){
    e.preventDefault();
    window.location.href = $(this).data('href');
  });
  //delete event
  $('.bt_delete').click(function(e){
    $.ajax({
      url: $(this).data('href'),
      type: 'get',
      success: function (data) {
        window.location.reload();
      },
      error: function (e) {
        console.log(e);
      }
    });
  });
});//ready func.
